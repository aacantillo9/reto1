package com.example.myprueba;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    private TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        texto = (TextView)findViewById(R.id.textView);

        Double base =getIntent().getDoubleExtra("base",0);

        Double area = base*5;

        texto.setText(String.valueOf(area));
    }
}
